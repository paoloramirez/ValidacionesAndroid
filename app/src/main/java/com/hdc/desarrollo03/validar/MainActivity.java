package com.hdc.desarrollo03.validar;

import android.annotation.SuppressLint;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {

    private Button btnValidar;
    private ImageView iconAlert;
    private EditText edtEmail;
    private EditText edtTelefono;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnValidar = findViewById(R.id.btnValidar);
        iconAlert = findViewById(R.id.iconAlert);
        edtEmail = findViewById(R.id.edtEmail);
        edtTelefono = findViewById(R.id.edtTelefono);

        iconAlert.setVisibility(View.INVISIBLE);

        btnValidar.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View view) {
                if (validarEmail(edtEmail.getText().toString()))
                {
                    iconAlert.setVisibility(View.INVISIBLE);

                }
                else {
                    iconAlert.setVisibility(View.VISIBLE);

                }
            }
        });


        edtTelefono.addTextChangedListener(new TextWatcher() {

            boolean cambiar=true;

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String tmp = charSequence.toString();
                String dato = tmp.replace("-","");

                if(dato.length()>=6 && cambiar==true && i2!=0)
                {
                    String s1 = dato.substring(0,3);
                    String s2 = dato.substring(3,6);
                    cambiar=false;
                    if (dato.length()==6)
                    {
                        edtTelefono.setText(s1+"-"+s2+"-");
                        edtTelefono.setSelection(i+2);
                    }
                    else
                    {
                        String s3 = dato.substring(6,dato.length());
                        edtTelefono.setText(s1+"-"+s2+"-"+s3);
                        edtTelefono.setSelection(i+1);
                    }
                    cambiar=true;
                }
                else
                {
                    if(dato.length()>=3 && cambiar==true && i2!=0)
                    {
                        String s1 = dato.substring(0,3);
                        cambiar=false;
                        if (dato.length()==3 && i2!=0)
                        {
                            edtTelefono.setText(s1+"-");
                            edtTelefono.setSelection(i+2);
                        }
                        else
                        {
                            String s2 = dato.substring(3,dato.length());
                            edtTelefono.setText(s1+"-"+s2);
                            edtTelefono.setSelection(i+1);
                        }

                        cambiar=true;
                    }
                }
            }


            @SuppressLint("NewApi")
            @Override
            public void afterTextChanged(Editable editable) {


            }
        });
    }


    private boolean validarEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }
}
